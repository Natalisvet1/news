
<div class="container animate-box">
		<div>
			<div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Новости</div>
		</div>"
		<div class="owl-carousel owl-theme" id="slider2">
<?php foreach ($elements as $item):
			$spisoc_cat="";
	foreach ($item['name_category'] as $category){
	$spisoc_cat.='<div class="d-block fh5co_small_post_heading">'.$category.'</div>';
	}?>
	<div class="item px-2">
		<div class="fh5co_hover_news_img">
			<div class="fh5co_news_img"><img src="/images/<?php echo $item['picture_name'][0]?><?php echo $item['extension'][0]?>" alt=""/></div>
			<div>
				<a href="group-<?php echo $item['name_group']?>" class="d-block fh5co_small_post_heading"><span class=""><?php echo $item['title']?></span></a>
				<div class="c_g"><i class="fa fa-clock-o"></i><?php echo $item['date']?></div>
			</div>
			<div>
				<div class="c_g">
					<a href="/group-<?php echo $item['name_group']?>" class="d-block fh5co_small_post_heading"><?php echo $item['name_group']?></a>
				</div>
				<div class="c_g"><?php echo $spisoc_cat?></div>
			</div>
		</div>
	</div>
<?php endforeach ?>
		</div>
</div>
</div>
