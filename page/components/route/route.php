<?php

/**
 * Class Route
 */
class Route
{
	/**
	 * @var array
	 */
	protected $uri;
	/**
	 * @var string[]
	 */
	private $segments;

	/**
	 * Route constructor.
	 */
	public function __construct()
	{
		$this->uri=parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		$this->segments = explode('/', trim($this->uri, '/'));
	}
	//маршрутизация
	public function rout(){
			switch ($this->segments[0]) {
				case 'about':
					require_once $_SERVER['DOCUMENT_ROOT'] . "/page/components/about/about.php";
					$controller = new About();
					$category=$controller->querySql();
					require_once $_SERVER['DOCUMENT_ROOT'] . "/page/shablon/about.php";
					break;
				case 'rules':
					require_once $_SERVER['DOCUMENT_ROOT']."/page/rules.php";
					break;
				case 'login':
					require_once $_SERVER['DOCUMENT_ROOT']."/page/components/user/log_in_to.php";
					break;
				case '':
					require_once $_SERVER['DOCUMENT_ROOT']."/page/components/news/news";
					$controller = new News();
					$elements=$controller->start();
					require_once $_SERVER['DOCUMENT_ROOT'] . "/page/shablon/news.php";
					if(!isset($_SESSION['user'])){
						require_once $_SERVER['DOCUMENT_ROOT']."/page/components/user/log_in_to.php";
					};
					break;
				case 'registr':
					require_once $_SERVER['DOCUMENT_ROOT']."/page/components/news/news";
					if(!isset($_SESSION['user'])){
						require_once $_SERVER['DOCUMENT_ROOT']."/page/components/user/user_registr.php";
					};
					break;
				default:
					/**
					 *  @var string[]
					 */
					$segment_group=explode('-', $this->segments[0]);
					if($segment_group[0]==='group'){
						switch (count($this->segments)) {
							case 1:
								require_once $_SERVER['DOCUMENT_ROOT']."/page/components/news/news";
								$controller = new News($segment_group[1]);
								$elements=$controller->start();
								require_once $_SERVER['DOCUMENT_ROOT'] . "/page/shablon/news.php";
								break;
							case 2:
								require_once $_SERVER['DOCUMENT_ROOT']."/page/components/news/news";
								$controller = new News($segment_group[1], $this->segments[1]);
								$elements=$controller->start();
								require_once $_SERVER['DOCUMENT_ROOT'] . "/page/shablon/news.php";
								break;
						}
					}
			}
	}

}