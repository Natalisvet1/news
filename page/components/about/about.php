<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/page/components/header/menu_top.php";

/**
 * Class About
 */
class About{
	/**
	 * @return PDO
	 */
	private function connectDataBase(){
	$connection = new PDO('mysql:host=localhost; dbname=news24;charset=utf8', 'root', 'root');
	return $connection;
}
	//делаем запрос к базе

	/**
	 * @return array
	 */
	public function querySql(){
		$query='SELECT ng.name_group, count(n.id_group) as count, ct.name as name_category
	FROM news as n
	inner join   news_group as ng on n.id_group=ng.id_group
	inner join category as ct on ct.id_news=n.id_news
	GROUP by n.id_group, ng.name_group, ct.name';
		$connection = $this->connectDataBase();
		$statement = $connection->prepare($query);
		$statement->execute();
		$rezult = $statement->fetchAll();
		//$this->shablonPrint($rezult);
		return $rezult;
	}


}