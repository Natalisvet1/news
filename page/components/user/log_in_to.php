<?php
if(isset($_POST['btn'])){
	require_once $_SERVER['DOCUMENT_ROOT'] . "/page/components/user/user_authentication.php";
	$controller = new user_authentication($_POST['log'],$_POST['psw'], $_POST['id_session']);
	$controller ->log_in();
}
?>
<form  method="Post" name="registerform">
	<div class="container">
		<h1>Войдите на сайт</h1>
		<hr>
		<label for="log"><b>Логин</b></label>
		<input type="text" placeholder="Логин" name="log" required>
		<input  type="hidden" name="id_session" value="<?php echo session_id()?>" >
		<label for="psw-repeat"><b>Пароль</b></label>
		<input type="password" placeholder="Пароль" name="psw" required>
		<hr>
		<button type="submit" class="registerbtn" name="btn">Войти</button>
	</div>
	<div class="container signin">
		<p>Не зарегистрированны? <a href="/registr/">Зарегистрироваться</a>.</p>
	</div>
</form>
